<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CartItem;
use App\Product;

class CartController extends Controller
{
    private function userCartItems($user_id)
    {
        $product = new \App\Product(1);
        return CartItem::where('user_id', $user_id)->get();
    }

    public function index(Request $request, $user_id)
    {
        return response()->json(['ok' => true, 'cart_items' => $this->userCartItems($user_id)] );
    }

    public function store(Request $request, $product_id, $user_id)
    {
        $existing_in_stock_items = $this->userCartItems($user_id)->filter(function($cart_item) use ($product_id) {
            return $cart_item->product()->id == $product_id;
        });
        $product = Product::findOrFail($product_id);
        if ( $existing_in_stock_items->isEmpty() ) {
            $cart_item = CartItem::create(['product_id' => $product_id, 'user_id' => $user_id]);
            return response()->json(['ok' => true, 'cart_item' => $cart_item] );
        }
        return response()->json(['ok' => true] );
    }

    public function delete(Request $request, $cart_id)
    {
        $existing_item = CartItem::findOrFail($cart_id);
        $existing_item->delete();
        return response()->json(['ok' => true, 'cart_item' => $existing_item] );
    }
}
