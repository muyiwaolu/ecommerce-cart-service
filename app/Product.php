<?php

namespace App;

class Product
{

    public static function findOrFail($product_id)
    {
        $product_service_uri = env('PRODUCT_SERVICE_URI') . '/'. $product_id;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $product_service_uri);
        if ($res->getStatusCode() != 200)
        {
            throw new \Exception('Cannot obtain product');
        }
        $product = json_decode($res->getBody()->getContents());
        return $product->data;
    }
}
