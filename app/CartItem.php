<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Product;

class CartItem extends Model
{
    protected $fillable = ['user_id', 'product_id'];

    public function product()
    {
        return Product::findOrFail($this->product_id);
    }
}
