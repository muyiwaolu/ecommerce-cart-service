<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/{user_id}', 'CartController@index');
$app->get('add/{product_id}/{user_id}', 'CartController@store');
$app->get('remove/{cart_id}', 'CartController@delete');
